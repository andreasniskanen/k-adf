k++ADF : SAT-based Reasoner for Ab­stract Dia­lect­ical Frame­works
===============================================================

Version    : 2021-03-31

Author     : [Andreas Niskanen](mailto:andreas.niskanen@helsinki.fi), University of Helsinki

Installation
------------

To compile the program:
```
make
```

To remove all object files:
```
make clean
```

Input format
------------

As input, `k++ADF` requires an ADF instance with the following syntax:

```
s(a).    # a is an argument
ac(a,f). # the acceptance condition of a is f
```

where `f` is a propositional formula, defined recursively via:

1. all arguments `a`, the truth constant `c(v)`, and the falsity constant `c(f)` are formulas,
2. all strings of the form `neg(p)`, `and(p,q)`, `or(p,q)`, `xor(p,q)`, `imp(p,q)`, and `iff(p,q)`, where `p` and `q` are subformulas, are formulas.

The predicates `neg`, `and`, `or`, `xor`, `imp`, and `iff` stand for negation, conjunction, disjunction, exclusive disjunction, implication, and equivalence, respectively.


Usage
-----

```
USAGE: ./k++adf <sem> [options] <file>

COMMAND LINE ARGUMENTS:
<sem>   : ADF semantics. <sem>={cf|nai|adm|com|prf|grd|mod|stb}
<file>  : Input filename for ADF instance.

COMMAND LINE OPTIONS:
-a <arg>: Query argument for acceptance problems.
-c      : Credulous acceptance. Requires the usage of -a option.
-s      : Skeptical acceptance. Requires the usage of -a option.
-l      : Output link types using att, sup, and dep predicates.
-f      : Do not use k-bipolar encodings.
-n      : Do not use shortcuts for skeptical acceptance.
-m      : Use complete semantics as an abstraction for preferred semantics.
-h      : Display this help message.
-v      : Display the version of the program.
```

Notes
-----

The SAT solver used in this software, [MiniSat](http://minisat.se/) (version 2.2.0), is included in this release.

Please direct any questions, comments, bug reports etc. directly to [the author](mailto:andreas.niskanen@helsinki.fi).


Reference
---------

Novel Algorithms for Abstract Dialectical Frameworks based on Complexity Analysis of Subclasses and SAT Solving.

Thomas Linsbichler, Marco Maratea, Andreas Niskanen, Johannes P. Wallner, and Stefan Woltran.

Proceedings of the Twenty-Seventh International Joint Conference on Artificial Intelligence.
Main track. Pages 1905-1911. [DOI](https://doi.org/10.24963/ijcai.2018/263)