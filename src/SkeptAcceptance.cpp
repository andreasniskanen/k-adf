/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "SkeptAcceptance.h"
#include "Constants.h"
#include "Encodings.h"
#include "Utils.h"
#include "CredAcceptance.h"
#include "Exists.h"

using namespace std;

namespace SkeptAcceptance {

bool skept_tvm(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);

	vector<int> statement_not_true_clause = {-adf.statement_var[statement]};
	solver.add_clause(statement_not_true_clause);

	two_valued_model(adf, solver);

	bool sat = solver.solve();
	return !sat;
}

bool skept_stb(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);

	vector<int> statement_not_true_clause = {-adf.statement_var[statement]};
	solver.add_clause(statement_not_true_clause);

	two_valued_model(adf, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_tv_interpretation(adf, solver.assignment);
		bool is_stable = true;
		vector<uint8_t> grounded(adf.statements.size(), Interpretation::Undefined);
		SAT_Solver new_solver = SAT_Solver(0);
		two_valued_model(adf, new_solver, false);
		for (uint32_t i = 0; i < adf.statements.size(); i++) {
			if (model[adf.statements[i]] == Interpretation::False) {
				vector<int> clause = { -adf.statement_var[adf.statements[i]] };
				new_solver.add_clause(clause);
				grounded[adf.statements[i]] = Interpretation::False;
			}
		}
		while (true) {
			bool fixpoint = true;
			vector<int> unit_clauses;
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (grounded[adf.statements[i]] != Interpretation::Undefined) {
					continue;
				}
				int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
				vector<int> not_unsat_clause = { val };
				bool not_unsat = new_solver.solve(not_unsat_clause);
				if (!not_unsat) {
					grounded[adf.statements[i]] = Interpretation::False;
					fixpoint = false;
					unit_clauses.push_back(-adf.statement_var[adf.statements[i]]);
					if (model[adf.statements[i]] == Interpretation::True) {
						is_stable = false;
						break;
					}
					continue;
				}
				vector<int> not_tautology_clause = { -val };
				bool not_tautology = new_solver.solve(not_tautology_clause);
				if (!not_tautology) {
					grounded[adf.statements[i]] = Interpretation::True;
					fixpoint = false;
					unit_clauses.push_back(adf.statement_var[adf.statements[i]]);
					continue;
				}
			}
			if (!is_stable || fixpoint) break;
			for (uint32_t i = 0; i < unit_clauses.size(); i++) {
				vector<int> clause = { unit_clauses[i] };
				new_solver.add_clause(clause);
			}
		}
		if (is_stable) {
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (model[adf.statements[i]] == Interpretation::True && grounded[adf.statements[i]] != Interpretation::True) {
					is_stable = false;
					break;
				}
			}
		}
		if (is_stable) {
			return false;
		}
		vector<int> refinement_clause = refine_tv_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return true;
}

bool skept_nai(const ADF & adf, const uint32_t statement) {	
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	vector<int> assumptions = {-adf.statement_true_var[statement]};

	while (true) {
		bool sat = solver.solve(assumptions);
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		vector<int> new_assumptions = assumptions;
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		cf_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve(new_assumptions);
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		new_assumptions[0] = -new_assumptions[0];
		if (!new_solver.solve(new_assumptions)) {
			return false;
		}
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return true;
}


bool skept_prf(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	if (adf.opt) {
		vector<uint8_t> grounded = Exists::compute_grd(adf);
		if (grounded[statement] == Interpretation::True) {
			return true;
		} else if (grounded[statement] == Interpretation::False) {
			return false;
		}
		valid_interpretation(adf, solver);
		cf_interpretation(adf, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, grounded, solver);
	} else {
		vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
		valid_interpretation(adf, solver);
		cf_interpretation(adf, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, interpretation, solver);
	}

	vector<int> assumptions = {-adf.statement_true_var[statement]};

	while (true) {
		bool sat = solver.solve(assumptions);
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(0);
		adm_verification(adf, model, new_solver);
		if (new_solver.solve()) {
			vector<int> refinement_clause = refine_neq_clause(adf, model);
			solver.add_clause(refinement_clause);
		} else {
			vector<int> new_assumptions = assumptions;
			SAT_Solver solver_ = SAT_Solver(2*(adf.statements.size()+1));
			valid_interpretation(adf, solver_);
			cf_interpretation(adf, solver_);
			bipolar_interpretation(adf, solver_);
			while (true) {
				larger_interpretation(adf, model, solver_);
				bool sat = solver_.solve(new_assumptions);
				if (!sat) break;
				vector<uint8_t> new_model = extract_interpretation(adf, solver_.assignment);
				SAT_Solver new_solver_ = SAT_Solver(0);
				adm_verification(adf, new_model, new_solver_);
				if (new_solver_.solve()) {
					vector<int> refinement_clause = refine_neq_clause(adf, new_model);
					solver_.add_clause(refinement_clause);
				} else {
					model = new_model;
				}
			}
			new_assumptions[0] = -new_assumptions[0];
			while (true) {
				if (!solver_.solve(new_assumptions)) {
					return false;
				}
				vector<uint8_t> new_model = extract_interpretation(adf, solver_.assignment);
				SAT_Solver new_solver_ = SAT_Solver(0);
				adm_verification(adf, new_model, new_solver_);
				if (new_solver_.solve()) {
					vector<int> refinement_clause = refine_neq_clause(adf, new_model);
					solver_.add_clause(refinement_clause);
				} else {
					break;
				}
			}
			vector<int> refinement_clause = refine_gt_clause(adf, model);
			solver.add_clause(refinement_clause);
		}
	}
	return true;
}

bool skept_prf_bipolar(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	if (adf.opt) {
		vector<uint8_t> grounded = Exists::compute_grd(adf);
		if (grounded[statement] == Interpretation::True) {
			return true;
		} else if (grounded[statement] == Interpretation::False) {
			return false;
		}
		valid_interpretation(adf, solver);
		cf_interpretation(adf, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, grounded, solver);
	} else {
		vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
		valid_interpretation(adf, solver);
		cf_interpretation(adf, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, interpretation, solver);
	}
	if (adf.com) {
		com_bipolar_interpretation(adf, solver);
	}

	vector<int> assumptions = {-adf.statement_true_var[statement]};

	while (true) {
		bool sat = solver.solve(assumptions);
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		vector<int> new_assumptions = assumptions;
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		cf_interpretation(adf, new_solver);
		bipolar_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve(new_assumptions);
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		new_assumptions[0] = -new_assumptions[0];
		if (!new_solver.solve(new_assumptions)) {
			return false;
		}
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return true;
}

bool skept_prf_k_bipolar(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	if (adf.opt) {
		vector<uint8_t> grounded = Exists::compute_grd(adf);
		if (grounded[statement] == Interpretation::True) {
			return true;
		} else if (grounded[statement] == Interpretation::False) {
			return false;
		}
		valid_interpretation(adf, solver);
		k_bipolar_interpretation(adf, grounded, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, grounded, solver);
	} else {
		vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
		valid_interpretation(adf, solver);
		k_bipolar_interpretation(adf, interpretation, solver);
		bipolar_interpretation(adf, solver);
		larger_interpretation(adf, interpretation, solver);
	}
	if (adf.com) {
		com_bipolar_interpretation(adf, solver);
	}

	vector<int> assumptions = {-adf.statement_true_var[statement]};

	while (true) {
		bool sat = solver.solve(assumptions);
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		vector<int> new_assumptions = assumptions;
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		k_bipolar_interpretation(adf, model, new_solver);
		bipolar_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve(new_assumptions);
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		new_assumptions[0] = -new_assumptions[0];
		if (!new_solver.solve(new_assumptions)) {
			return false;
		}
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return true;
}

}