/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#if defined(SAT_MINISAT)
#include "MiniSATSolver.h"
typedef MiniSATSolver SAT_Solver;
#else
#error "No SAT solver defined"
#endif

#include "Exists.h"
#include "Constants.h"
#include "Utils.h"

using namespace std;

namespace Exists {

vector<uint8_t> compute_grd(const ADF & adf) {
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);

	vector<vector<int>> base_clauses = {{adf.true_var}, {-adf.false_var}};
	SAT_Solver solver = SAT_Solver(0);
	solver.add_clauses(base_clauses);
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		solver.add_clauses(adf.statement_clauses[i]);
	}
	while (true) {
		vector<uint8_t> new_interpretation(adf.statements.size(), Interpretation::Undefined);
		for (uint32_t i = 0; i < adf.statements.size(); i++) {
			int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
			vector<int> not_tautology_clause = { -val };
			bool not_tautology = solver.solve(not_tautology_clause);
			if (!not_tautology) {
				new_interpretation[i] = Interpretation::True;
				continue;
			}
			vector<int> not_unsat_clause = { val };
			bool not_unsat = solver.solve(not_unsat_clause);
			if (!not_unsat) {
				new_interpretation[i] = Interpretation::False;
				continue;
			}
		}
		bool fixpoint = true;
		for (uint32_t i = 0; i < adf.statements.size(); i++) {
			if (interpretation[i] != new_interpretation[i]) {
				fixpoint = false;
				if (new_interpretation[i] == Interpretation::True) {
					vector<int> clause = { adf.statement_var[adf.statements[i]] };
					solver.add_clause(clause);
				} else if (new_interpretation[i] == Interpretation::False) {
					vector<int> clause = { -adf.statement_var[adf.statements[i]] };
					solver.add_clause(clause);
				}
			}
		}
		if (fixpoint) {
			break;
		} else {
			interpretation = new_interpretation;
		}
	}
	return interpretation;
}

}