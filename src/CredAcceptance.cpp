/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "CredAcceptance.h"
#include "Constants.h"
#include "Encodings.h"
#include "Utils.h"

using namespace std;

namespace CredAcceptance {

bool cred_tvm(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);

	vector<int> statement_true_clause = {adf.statement_var[statement]};
	solver.add_clause(statement_true_clause);

	two_valued_model(adf, solver);

	bool sat = solver.solve();
	return sat;
}

bool cred_stb(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);

	vector<int> statement_true_clause = {adf.statement_var[statement]};
	solver.add_clause(statement_true_clause);

	two_valued_model(adf, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_tv_interpretation(adf, solver.assignment);
		bool is_stable = true;
		vector<uint8_t> grounded(adf.statements.size(), Interpretation::Undefined);
		SAT_Solver new_solver = SAT_Solver(0);
		two_valued_model(adf, new_solver, false);
		for (uint32_t i = 0; i < adf.statements.size(); i++) {
			if (model[adf.statements[i]] == Interpretation::False) {
				vector<int> clause = { -adf.statement_var[adf.statements[i]] };
				new_solver.add_clause(clause);
				grounded[adf.statements[i]] = Interpretation::False;
			}
		}
		while (true) {
			bool fixpoint = true;
			vector<int> unit_clauses;
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (grounded[adf.statements[i]] != Interpretation::Undefined) {
					continue;
				}
				int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
				vector<int> not_unsat_clause = { val };
				bool not_unsat = new_solver.solve(not_unsat_clause);
				if (!not_unsat) {
					grounded[adf.statements[i]] = Interpretation::False;
					fixpoint = false;
					unit_clauses.push_back(-adf.statement_var[adf.statements[i]]);
					if (model[adf.statements[i]] == Interpretation::True) {
						is_stable = false;
						break;
					}
					continue;
				}
				vector<int> not_tautology_clause = { -val };
				bool not_tautology = new_solver.solve(not_tautology_clause);
				if (!not_tautology) {
					grounded[adf.statements[i]] = Interpretation::True;
					fixpoint = false;
					unit_clauses.push_back(adf.statement_var[adf.statements[i]]);
					continue;
				}
			}
			if (!is_stable || fixpoint) break;
			for (uint32_t i = 0; i < unit_clauses.size(); i++) {
				vector<int> clause = { unit_clauses[i] };
				new_solver.add_clause(clause);
			}
		}
		if (is_stable) {
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (model[adf.statements[i]] == Interpretation::True && grounded[adf.statements[i]] != Interpretation::True) {
					is_stable = false;
					break;
				}
			}
		}
		if (is_stable) {
			return true;
		}
		vector<int> refinement_clause = refine_tv_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return false;
}

bool cred_cf(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	vector<int> statement_true_clause = {adf.statement_true_var[statement]};
	solver.add_clause(statement_true_clause);
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	
	bool sat = solver.solve();
	return sat;
}

bool cred_adm(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	vector<int> statement_true_clause = {adf.statement_true_var[statement]};
	solver.add_clause(statement_true_clause);
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	
	while (true) {
		bool sat = solver.solve();
		if (!sat) break;

		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);

		SAT_Solver new_solver = SAT_Solver(0);
		adm_verification(adf, model, new_solver);

		if (new_solver.solve()) {
			vector<int> refinement_clause = refine_neq_clause(adf, model);
			solver.add_clause(refinement_clause);
		} else {
			return true;
		}
	}
	return false;
}

bool cred_adm_bipolar(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));

	vector<int> statement_true_clause = {adf.statement_true_var[statement]};
	solver.add_clause(statement_true_clause);
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	
	bool sat = solver.solve();
	return sat;
}

bool cred_adm_k_bipolar(const ADF & adf, const uint32_t statement) {
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);

	vector<int> statement_true_clause = {adf.statement_true_var[statement]};
	solver.add_clause(statement_true_clause);
	
	valid_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	k_bipolar_interpretation(adf, interpretation, solver);

	bool sat = solver.solve();
	return sat;
}

}