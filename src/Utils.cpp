/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Constants.h"
#include "Utils.h"

#include <iostream>

using namespace std;

vector<uint8_t> extract_interpretation(const ADF & adf, const vector<uint8_t> & assignment) {
	vector<uint8_t> interpretation(adf.statements.size());
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		if (assignment[adf.statement_true_var[adf.statements[i]]-1]) {
			interpretation[i] = Interpretation::True;
		} else if (assignment[adf.statement_false_var[adf.statements[i]]-1]) {
			interpretation[i] = Interpretation::False;
		} else {
			interpretation[i] = Interpretation::Undefined;
		}
	}
	return interpretation;
}

vector<uint8_t> extract_tv_interpretation(const ADF & adf, const vector<uint8_t> & assignment) {
	vector<uint8_t> interpretation(adf.statements.size());
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		if (assignment[adf.statement_var[adf.statements[i]]-1]) {
			interpretation[i] = Interpretation::True;
		} else {
			interpretation[i] = Interpretation::False;
		}
	}
	return interpretation;
}

vector<int> refine_gt_clause(const ADF & adf, const vector<uint8_t> & interpretation) {
	vector<int> clause;
	clause.reserve(adf.statements.size());
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		if (interpretation[i] == Interpretation::True) {
			clause.push_back(adf.statement_false_var[adf.statements[i]]);
		} else if (interpretation[i] == Interpretation::False) {
			clause.push_back(adf.statement_true_var[adf.statements[i]]);
		} else {
			clause.push_back(adf.statement_true_var[adf.statements[i]]);
			clause.push_back(adf.statement_false_var[adf.statements[i]]);
		}
	}
	return clause;
}

vector<int> refine_neq_clause(const ADF & adf, const vector<uint8_t> & interpretation) {
	vector<int> clause;
	clause.reserve(adf.statements.size());
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		if (interpretation[i] == Interpretation::True) {
			clause.push_back(-adf.statement_true_var[adf.statements[i]]);
		} else if (interpretation[i] == Interpretation::False) {
			clause.push_back(-adf.statement_false_var[adf.statements[i]]);
		} else {
			clause.push_back(adf.statement_true_var[adf.statements[i]]);
			clause.push_back(adf.statement_false_var[adf.statements[i]]);
		}
	}
	return clause;
}

vector<int> refine_tv_clause(const ADF & adf, const vector<uint8_t> & interpretation) {
	vector<int> clause(adf.statements.size());
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		if (interpretation[i] == Interpretation::True) {
			clause[i] = -adf.statement_var[adf.statements[i]];
		} else {
			clause[i] = adf.statement_var[adf.statements[i]];
		}
	}
	return clause;
}

void print_clause(const vector<int> & v) {
	for (uint32_t i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}
	cout << "0\n";
}

void print_clauses(const vector<vector<int>> & v) {
	for (uint32_t i = 0; i < v.size(); i++) {
		print_clause(v[i]);
	}
}

void print_interpretation(const ADF & adf, const vector<uint8_t> & v) {
	for (uint32_t i = 0; i < v.size(); i++) {
		if (v[i] == Interpretation::True) {
        	cout << "t(" << adf.idx_to_statement.at(adf.statements[i]) << ") ";
        }
        /*else if (v[i] == Interpretation::False) {
        	std::cout << "f(" << adf.idx_to_statement[adf.statements[i]] << ") ";
        } else {
        	std::cout << "u(" << adf.idx_to_statement[adf.statements[i]] << ") ";
        }*/
	}
	for (uint32_t i = 0; i < v.size(); i++) {
		if (v[i] == Interpretation::False) {
        	cout << "f(" << adf.idx_to_statement.at(adf.statements[i]) << ") ";
        }
    }
    for (uint32_t i = 0; i < v.size(); i++) {
		if (v[i] == Interpretation::Undefined) {
        	cout << "u(" << adf.idx_to_statement.at(adf.statements[i]) << ") ";
        }
    }
	cout << "\n";
}