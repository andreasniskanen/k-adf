/*!
 * Copyright (c) <2018> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Encodings.h"
#include "Constants.h"

using namespace std;

void two_valued_model(const ADF & adf, SAT_Solver & solver, bool equivalence) {
	vector<int> true_clause = { adf.true_var };
	vector<int> false_clause = { -adf.false_var };
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		solver.add_clauses(adf.statement_clauses[i]);
		if (equivalence) {
			vector<int> t_clause = { -adf.statement_var[adf.statements[i]], (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value };
			solver.add_clause(t_clause);
			vector<int> f_clause = { adf.statement_var[adf.statements[i]], -(adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value };
			solver.add_clause(f_clause);
		}
	}
}

void valid_interpretation(const ADF & adf, SAT_Solver & solver) {
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		vector<int> clause = { -adf.statement_true_var[adf.statements[i]], -adf.statement_false_var[adf.statements[i]] };
		solver.add_clause(clause);
	}
}

void larger_interpretation(const ADF & adf, const std::vector<uint8_t> & interpretation, SAT_Solver & solver) {
	vector<int> undef_clause;
	for (uint32_t i = 0; i < interpretation.size(); i++) {
		if (interpretation[i] == Interpretation::True) {
			vector<int> clause = { adf.statement_true_var[adf.statements[i]] };
			solver.add_clause(clause);
		} else if (interpretation[i] == Interpretation::False) {
			vector<int> clause = { adf.statement_false_var[adf.statements[i]] };
			solver.add_clause(clause);
		} else {
			undef_clause.push_back(adf.statement_true_var[adf.statements[i]]);
			undef_clause.push_back(adf.statement_false_var[adf.statements[i]]);
		}
	}
	solver.add_clause(undef_clause);
}

void cf_interpretation(const ADF & adf, SAT_Solver & solver) {
	vector<int> true_clause = { adf.true_var };
	vector<int> false_clause = { -adf.false_var };
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		solver.add_clauses(adf.condition_clauses[i]);
		vector<int> t_clause = { -adf.statement_true_var[adf.statements[i]], (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->value };
		solver.add_clause(t_clause);
		vector<int> f_clause = { -adf.statement_false_var[adf.statements[i]], -(adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->value };
		solver.add_clause(f_clause);
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]], adf.link_var.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]], -adf.link_var.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
	}
}

void adm_verification(const ADF & adf, const vector<uint8_t> & interpretation, SAT_Solver & solver) {
	vector<int> true_clause = { adf.true_var };
	vector<int> false_clause = { -adf.false_var };
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);
	for (uint32_t i = 0; i < adf.statement_clauses.size(); i++) {
		solver.add_clauses(adf.statement_clauses[i]);
	}
	for (uint32_t i = 0; i < interpretation.size(); i++) {
		if (interpretation[i] == Interpretation::True) {
			vector<int> clause = { adf.statement_var[adf.statements[i]] };
			solver.add_clause(clause);
		} else if (interpretation[i] == Interpretation::False) {
			vector<int> clause = { -adf.statement_var[adf.statements[i]] };
			solver.add_clause(clause);
		}
	}
	vector<int> not_adm_clause;
	for (uint32_t i = 0; i < interpretation.size(); i++) {
		int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
		if (interpretation[i] == Interpretation::True) {
			not_adm_clause.push_back(-val);
		} else if (interpretation[i] == Interpretation::False) {
			not_adm_clause.push_back(val);
		}
	}
	solver.add_clause(not_adm_clause);
}

void bipolar_interpretation(const ADF & adf, SAT_Solver & solver) {
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		for (uint32_t j = 0; j < adf.att_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]], 
			                        adf.statement_false_var[adf.att_parents[adf.statements[i]][j]],
			                        adf.link_var.at(make_pair(adf.att_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.sup_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]],
			                        adf.statement_true_var[adf.sup_parents[adf.statements[i]][j]],
			                       -adf.link_var.at(make_pair(adf.sup_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.att_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]],
			                        adf.statement_true_var[adf.att_parents[adf.statements[i]][j]],
			                       -adf.link_var.at(make_pair(adf.att_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.sup_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]],
			                        adf.statement_false_var[adf.sup_parents[adf.statements[i]][j]],
			                        adf.link_var.at(make_pair(adf.sup_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
	}
}

void com_bipolar_interpretation(const ADF & adf, SAT_Solver & solver) {
	int count = 0;
	unordered_map<pair<uint32_t,uint32_t>,int> link_to_var_1;
	unordered_map<pair<uint32_t,uint32_t>,int> link_to_var_2;
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		int root_value_1 = 0;
		map<int,int> var_to_var_1;
		for (uint32_t j = 0; j < adf.condition_clauses[adf.statements[i]].size(); j++) {
			vector<int> clause;
			for (uint32_t k = 0; k < adf.condition_clauses[adf.statements[i]][j].size(); k++) {
				int current_var = abs(adf.condition_clauses[i][j][k]);
				if (current_var == adf.true_var || current_var == adf.false_var) {
					if (current_var == adf.conditions[i].root->value) {
						root_value_1 = current_var;
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? current_var : -current_var);
				} else if (adf.link_var_to_pair.find(current_var) == adf.link_var_to_pair.end()) {
					if (var_to_var_1.find(current_var) == var_to_var_1.end()) {
						var_to_var_1[current_var] = solver.n_vars + ++count;
						if (current_var == adf.conditions[i].root->value) {
							root_value_1 = var_to_var_1[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? var_to_var_1[current_var] : -var_to_var_1[current_var]);
				} else {
					pair<uint32_t,uint32_t> link = adf.link_var_to_pair.at(current_var);
					if (var_to_var_1.find(current_var) == var_to_var_1.end()) {
						var_to_var_1[current_var] = solver.n_vars + ++count;
						link_to_var_1[link] = var_to_var_1[current_var];
						if (current_var == adf.conditions[i].root->value) {
							root_value_1 = var_to_var_1[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? var_to_var_1[current_var] : -var_to_var_1[current_var]);
				}
			}
			solver.add_clause(clause);
		}
		if (adf.condition_clauses[i].size() == 0) {
			int current_var = adf.conditions[i].root->value;
			if (current_var != adf.true_var && current_var != adf.false_var) {
				link_to_var_1[adf.link_var_to_pair.at(current_var)] = solver.n_vars + ++count;
				root_value_1 = solver.n_vars + count;
			} else {
				root_value_1 = current_var;
			}
		}
		for (uint32_t j = 0; j < adf.att_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = {  adf.statement_false_var[adf.att_parents[adf.statements[i]][j]],
			                        link_to_var_1.at(make_pair(adf.att_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.sup_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = {  adf.statement_true_var[adf.sup_parents[adf.statements[i]][j]],
			                       -link_to_var_1.at(make_pair(adf.sup_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		vector<int> true_clause = { -(adf.conditions[i].root->neg ? -1 : 1) * root_value_1, adf.statement_true_var[adf.statements[i]] };
		solver.add_clause(true_clause);
		int root_value_2 = 0;
		map<int,int> var_to_var_2;
		for (uint32_t j = 0; j < adf.condition_clauses[adf.statements[i]].size(); j++) {
			vector<int> clause;
			for (uint32_t k = 0; k < adf.condition_clauses[adf.statements[i]][j].size(); k++) {
				int current_var = abs(adf.condition_clauses[i][j][k]);
				if (current_var == adf.true_var || current_var == adf.false_var) {
					if (current_var == adf.conditions[i].root->value) {
						root_value_2 = current_var;
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? current_var : -current_var);
				} else if (adf.link_var_to_pair.find(current_var) == adf.link_var_to_pair.end()) {
					if (var_to_var_2.find(current_var) == var_to_var_2.end()) {
						var_to_var_2[current_var] = solver.n_vars + ++count;
						if (current_var == adf.conditions[i].root->value) {
							root_value_2 = var_to_var_2[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? var_to_var_2[current_var] : -var_to_var_2[current_var]);
				} else {
					pair<uint32_t,uint32_t> link = adf.link_var_to_pair.at(current_var);
					if (var_to_var_2.find(current_var) == var_to_var_2.end()) {
						var_to_var_2[current_var] = solver.n_vars + ++count;
						link_to_var_2[link] = var_to_var_2[current_var];
						if (current_var == adf.conditions[i].root->value) {
							root_value_2 = var_to_var_2[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[i][j][k] > 0) ? var_to_var_2[current_var] : -var_to_var_2[current_var]);
				}
			}
			solver.add_clause(clause);
		}
		if (adf.condition_clauses[i].size() == 0) {
			int current_var = adf.conditions[i].root->value;
			if (current_var != adf.true_var && current_var != adf.false_var) {
				link_to_var_2[adf.link_var_to_pair.at(current_var)] = solver.n_vars + ++count;
				root_value_2 = solver.n_vars + count;
			} else {
				root_value_2 = current_var;
			}
		}
		for (uint32_t j = 0; j < adf.att_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = {  adf.statement_true_var[adf.att_parents[adf.statements[i]][j]],
			                       -link_to_var_2.at(make_pair(adf.att_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.sup_parents[adf.statements[i]].size(); j++) {
			vector<int> clause = {  adf.statement_false_var[adf.sup_parents[adf.statements[i]][j]],
			                        link_to_var_2.at(make_pair(adf.sup_parents[adf.statements[i]][j], adf.statements[i])) };
			solver.add_clause(clause);
		}
		vector<int> false_clause = { (adf.conditions[i].root->neg ? -1 : 1) * root_value_2, adf.statement_false_var[adf.statements[i]] };
		solver.add_clause(false_clause);
	}
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]], link_to_var_1.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]], -link_to_var_1.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]], link_to_var_2.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]], -link_to_var_2.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
	}
}

void k_bipolar_interpretation(const ADF & adf, const vector<uint8_t> & interpretation, SAT_Solver & solver) {
	vector<int> true_clause = { adf.true_var };
	vector<int> false_clause = { -adf.false_var };
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);

	int count = 0;
	for (uint32_t i = 0; i < adf.statements.size(); i++) {
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_true_var[adf.statements[i]], adf.link_var.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}
		for (uint32_t j = 0; j < adf.range[adf.statements[i]].size(); j++) {
			vector<int> clause = { -adf.statement_false_var[adf.statements[i]], -adf.link_var.at(make_pair(adf.statements[i], adf.range[adf.statements[i]][j])) };
			solver.add_clause(clause);
		}

		vector<uint32_t> non_bip_parents;
		vector<uint32_t> non_bip_parent_to_idx(adf.statements.size(), 0);
		vector<uint8_t> is_non_bip_parent(adf.statements.size(), 0);

		for (uint32_t j = 0; j < adf.non_bipolar_parents[adf.statements[i]].size(); j++) {
			if (interpretation[adf.non_bipolar_parents[adf.statements[i]][j]] == Interpretation::Undefined) {
				non_bip_parent_to_idx[adf.non_bipolar_parents[adf.statements[i]][j]] = non_bip_parents.size();
				non_bip_parents.push_back(adf.non_bipolar_parents[adf.statements[i]][j]);
				is_non_bip_parent[adf.non_bipolar_parents[adf.statements[i]][j]] = 1;
			}
		}

		int k = non_bip_parents.size();
		if (k == 0) {
			solver.add_clauses(adf.condition_clauses[i]);
			vector<int> t_clause = { -adf.statement_true_var[adf.statements[i]], (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->value };
			solver.add_clause(t_clause);
			vector<int> f_clause = { -adf.statement_false_var[adf.statements[i]], -(adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->value };
			solver.add_clause(f_clause);
			continue;
		}

		for (uint32_t j = 0; j < (1<<k); j++) {
			int root_value = 0;
			map<int,int> var_to_new_var;
			for (uint32_t l = 0; l < adf.condition_clauses[i].size(); l++) {
				vector<int> clause;
				for (uint32_t m = 0; m < adf.condition_clauses[i][l].size(); m++) {
					int current_var = abs(adf.condition_clauses[i][l][m]);
					if (current_var == adf.true_var || current_var == adf.false_var) {
						if (current_var == adf.conditions[i].root->value) {
							root_value = current_var;
						}
						clause.push_back((adf.condition_clauses[i][l][m] > 0) ? current_var : -current_var);
					} else if (adf.link_var_to_pair.find(current_var) == adf.link_var_to_pair.end()) {
						if (var_to_new_var.find(current_var) == var_to_new_var.end()) {
							var_to_new_var[current_var] = adf.count + ++count;
							if (current_var == adf.conditions[i].root->value) {
								root_value = var_to_new_var[current_var];
							}
						}
						clause.push_back((adf.condition_clauses[i][l][m] > 0) ? var_to_new_var[current_var] : -var_to_new_var[current_var]);
					} else {
						pair<uint32_t,uint32_t> link = adf.link_var_to_pair.at(current_var);
						if (is_non_bip_parent[link.first]) {
							if (j & (1<<non_bip_parent_to_idx[link.first])) {
								clause.push_back((adf.condition_clauses[i][l][m] > 0) ? adf.true_var : -adf.true_var);
							} else {
								clause.push_back((adf.condition_clauses[i][l][m] > 0) ? adf.false_var : -adf.false_var);
							}
						} else {
							clause.push_back((adf.condition_clauses[i][l][m] > 0) ? current_var : -current_var);
						}
					}
				}
				solver.add_clause(clause);
			}

			int val = (adf.conditions[i].root-> neg ? -1 : 1) * root_value;
			vector<int> clause;
			clause.push_back(-adf.statement_true_var[adf.statements[i]]);
			for (uint32_t k = 0; k < non_bip_parents.size(); k++) {
				if (j & (1 << k)) {
					clause.push_back(adf.statement_false_var[non_bip_parents[k]]);
				} else {
					clause.push_back(adf.statement_true_var[non_bip_parents[k]]);
				}
			}
			clause.push_back(val);
			solver.add_clause(clause);
			clause.clear();
			clause.push_back(-adf.statement_false_var[adf.statements[i]]);
			for (uint32_t k = 0; k < non_bip_parents.size(); k++) {
				if (j & (1 << k)) {
					clause.push_back(adf.statement_false_var[non_bip_parents[k]]);
				} else {
					clause.push_back(adf.statement_true_var[non_bip_parents[k]]);
				}
			}
			clause.push_back(-val);
			solver.add_clause(clause);
		}
	}
}