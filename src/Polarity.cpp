/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Polarity.h"

#if defined(SAT_MINISAT)
#include "MiniSATSolver.h"
typedef MiniSATSolver SAT_Solver;
#else
#error "No SAT solver defined"
#endif

using namespace std;

namespace Polarity {

bool link_is_sup(const ADF & adf, const pair<uint32_t,uint32_t> link) {
	SAT_Solver solver = SAT_Solver(0);
	
	vector<int> true_clause = {adf.true_var};
	vector<int> false_clause = {-adf.false_var};
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);
	
	int b = link.first;
	int a = link.second;
	solver.add_clauses(adf.statement_clauses[a]);
	int adf_vars = adf.statements.size() + 2;
	
	if (adf.statement_clauses[a].size() != 0) {
		int count = 0;
		int root_value_flipped = 0;
		map<int,int> var_to_flipped_var;
		for (uint32_t i = 0; i < adf.statement_clauses[a].size(); i++) {
			vector<int> clause;
			for (uint32_t j = 0; j < adf.statement_clauses[a][i].size(); j++) {
				int current_var = abs(adf.statement_clauses[a][i][j]);
				if (current_var > adf_vars) {
					if (var_to_flipped_var.find(current_var) == var_to_flipped_var.end()) {
						var_to_flipped_var[current_var] = solver.n_vars + ++count;
						if (current_var == adf.conditions[a].root->global_value) {
							root_value_flipped = var_to_flipped_var[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[a][i][j] > 0) ? var_to_flipped_var[current_var] : - var_to_flipped_var[current_var]);
				} else {
					if (current_var == adf.statement_var[b]) {
						current_var = -current_var;
					}
					clause.push_back((adf.statement_clauses[a][i][j] > 0) ? current_var : -current_var);
				}
			}
			solver.add_clause(clause);
		}
		vector<int> clause = { -adf.statement_var[b] };
		solver.add_clause(clause);
		clause = { (adf.conditions[a].root->neg ? -1 : 1)*adf.conditions[a].root->global_value };
		solver.add_clause(clause);
		clause = { -(adf.conditions[a].root->neg ? -1 : 1)*root_value_flipped };
		solver.add_clause(clause);
	} else {
		vector<int> clause = { -adf.statement_var[b] };
		solver.add_clause(clause);
		clause = { (adf.conditions[a].root->neg ? -1 : 1)*adf.conditions[a].root->global_value };
		solver.add_clause(clause);
	}
	bool sat = solver.solve();
	return !sat;
}

bool link_is_att(const ADF & adf, const pair<uint32_t,uint32_t> link) {
	SAT_Solver solver = SAT_Solver(0);
	
	vector<int> true_clause = {adf.true_var};
	vector<int> false_clause = {-adf.false_var};
	solver.add_clause(true_clause);
	solver.add_clause(false_clause);
	
	int b = link.first;
	int a = link.second;
	solver.add_clauses(adf.statement_clauses[a]);
	int adf_vars = adf.statements.size() + 2;
	
	if (adf.statement_clauses[a].size() != 0) {
		int count = 0;
		int root_value_flipped = 0;
		map<int,int> var_to_flipped_var;
		for (uint32_t i = 0; i < adf.statement_clauses[a].size(); i++) {
			vector<int> clause;
			for (uint32_t j = 0; j < adf.statement_clauses[a][i].size(); j++) {
				int current_var = abs(adf.statement_clauses[a][i][j]);
				if (current_var > adf_vars) {
					if (var_to_flipped_var.find(current_var) == var_to_flipped_var.end()) {
						var_to_flipped_var[current_var] = solver.n_vars + ++count;
						if (current_var == adf.conditions[a].root->global_value) {
							root_value_flipped = var_to_flipped_var[current_var];
						}
					}
					clause.push_back((adf.statement_clauses[a][i][j] > 0) ? var_to_flipped_var[current_var] : - var_to_flipped_var[current_var]);
				} else {
					if (current_var == adf.statement_var[b]) {
						current_var = -current_var;
					}
					clause.push_back((adf.statement_clauses[a][i][j] > 0) ? current_var : -current_var);
				}
			}
			solver.add_clause(clause);
		}
		vector<int> clause = { -adf.statement_var[b] };
		solver.add_clause(clause);
		clause = { -(adf.conditions[a].root->neg ? -1 : 1)*adf.conditions[a].root->global_value };
		solver.add_clause(clause);
		clause = { (adf.conditions[a].root->neg ? -1 : 1)*root_value_flipped };
		solver.add_clause(clause);
	} else {
		vector<int> clause = { -adf.statement_var[b] };
		solver.add_clause(clause);
		clause = { -(adf.conditions[a].root->neg ? -1 : 1)*adf.conditions[a].root->global_value };
		solver.add_clause(clause);
	}
	bool sat = solver.solve();
	return !sat;
}

}