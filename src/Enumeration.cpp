/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Enumeration.h"
#include "Constants.h"
#include "Encodings.h"
#include "Utils.h"
#include "Exists.h"

using namespace std;

namespace Enumeration {

vector<vector<uint8_t>> enumerate_tvm(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;

	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);
	two_valued_model(adf, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_tv_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_tv_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_stb(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;

	SAT_Solver solver = SAT_Solver(adf.statements.size()+2);
	two_valued_model(adf, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_tv_interpretation(adf, solver.assignment);
		bool is_stable = true;
		vector<uint8_t> grounded(adf.statements.size(), Interpretation::Undefined);
		SAT_Solver new_solver = SAT_Solver(0);
		two_valued_model(adf, new_solver, false);
		for (uint32_t i = 0; i < adf.statements.size(); i++) {
			if (model[adf.statements[i]] == Interpretation::False) {
				vector<int> clause = { -adf.statement_var[adf.statements[i]] };
				new_solver.add_clause(clause);
				grounded[adf.statements[i]] = Interpretation::False;
			}
		}
		while (true) {
			bool fixpoint = true;
			vector<int> unit_clauses;
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (grounded[adf.statements[i]] != Interpretation::Undefined) {
					continue;
				}
				int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
				vector<int> not_unsat_clause = { val };
				bool not_unsat = new_solver.solve(not_unsat_clause);
				if (!not_unsat) {
					grounded[adf.statements[i]] = Interpretation::False;
					fixpoint = false;
					unit_clauses.push_back(-adf.statement_var[adf.statements[i]]);
					if (model[adf.statements[i]] == Interpretation::True) {
						is_stable = false;
						break;
					}
					continue;
				}
				vector<int> not_tautology_clause = { -val };
				bool not_tautology = new_solver.solve(not_tautology_clause);
				if (!not_tautology) {
					grounded[adf.statements[i]] = Interpretation::True;
					fixpoint = false;
					unit_clauses.push_back(adf.statement_var[adf.statements[i]]);
					continue;
				}
			}
			if (!is_stable || fixpoint) break;
			for (uint32_t i = 0; i < unit_clauses.size(); i++) {
				vector<int> clause = { unit_clauses[i] };
				new_solver.add_clause(clause);
			}
		}
		if (is_stable) {
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (model[adf.statements[i]] == Interpretation::True && grounded[adf.statements[i]] != Interpretation::True) {
					is_stable = false;
					break;
				}
			}
		}
		if (is_stable) {
			interpretations.push_back(model);
		}
		vector<int> refinement_clause = refine_tv_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_cf(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);
	
	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_nai(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
	
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		cf_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve();
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	if (interpretations.size() == 0) {
		interpretations.push_back(interpretation);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_adm(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(0);
		adm_verification(adf, model, new_solver);
		if (!new_solver.solve()) {
			interpretations.push_back(model);
		}
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_com(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);
	interpretations.push_back(interpretation);
	
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	vector<vector<int>> base_clauses = {{adf.true_var}, {-adf.false_var}};

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(0);
		adm_verification(adf, model, new_solver);
		if (!new_solver.solve()) {
			SAT_Solver yet_another_solver = SAT_Solver(0);
			yet_another_solver.add_clauses(base_clauses);
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (model[i] == Interpretation::True) {
					vector<int> clause = { adf.statement_var[adf.statements[i]] };
					yet_another_solver.add_clause(clause);
				} else if (model[i] == Interpretation::False) {
					vector<int> clause = { -adf.statement_var[adf.statements[i]] };
					yet_another_solver.add_clause(clause);
				}
			}
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				yet_another_solver.add_clauses(adf.statement_clauses[i]);
			}
			while (true) {
				vector<uint8_t> new_model(adf.statements.size(), Interpretation::Undefined);
				for (uint32_t i = 0; i < adf.statements.size(); i++) {
					int val = (adf.conditions[i].root->neg ? -1 : 1) * adf.conditions[i].root->global_value;
					vector<int> not_tautology_clause = {-val};
					bool not_tautology = yet_another_solver.solve(not_tautology_clause);
					if (!not_tautology) {
						new_model[i] = Interpretation::True;
						continue;
					}
					vector<int> not_unsat_clause = {val};
					bool not_unsat = yet_another_solver.solve(not_unsat_clause);
					if (!not_unsat) {
						new_model[i] = Interpretation::False;
						continue;
					}
				}
				bool fixpoint = true;
				for (uint32_t i = 0; i < adf.statements.size(); i++) {
					if (model[i] != new_model[i]) {
						fixpoint = false;
						if (new_model[i] == Interpretation::True) {
							vector<int> clause = { adf.statement_var[adf.statements[i]] };
							yet_another_solver.add_clause(clause);
						} else if (new_model[i] == Interpretation::False) {
							vector<int> clause = { -adf.statement_var[adf.statements[i]] };
							yet_another_solver.add_clause(clause);
						}
					}
				}
				if (fixpoint) {
					break;
				} else {
					vector<int> refinement_clause = refine_neq_clause(adf, model);
					solver.add_clause(refinement_clause);
					model = new_model;
				}
			}
			vector<int> assumptions;
			for (uint32_t i = 0; i < adf.statements.size(); i++) {
				if (model[i] == Interpretation::True) {
					assumptions.push_back(adf.statement_true_var[adf.statements[i]]);
					assumptions.push_back(-adf.statement_false_var[adf.statements[i]]);
				} else if (model[i] == Interpretation::False) {
					assumptions.push_back(adf.statement_false_var[adf.statements[i]]);
					assumptions.push_back(-adf.statement_true_var[adf.statements[i]]);
				} else {
					assumptions.push_back(-adf.statement_true_var[adf.statements[i]]);
					assumptions.push_back(-adf.statement_false_var[adf.statements[i]]);
				}
			}
			if (solver.solve(assumptions)) {
				interpretations.push_back(model);
				vector<int> refinement_clause = refine_neq_clause(adf, model);
				solver.add_clause(refinement_clause);
			}
		} else {
			vector<int> refinement_clause = refine_neq_clause(adf, model);
			solver.add_clause(refinement_clause);
		}
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_prf(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(0);
		adm_verification(adf, model, new_solver);
		if (new_solver.solve()) {
			vector<int> refinement_clause = refine_neq_clause(adf, model);
			solver.add_clause(refinement_clause);
		} else {
			SAT_Solver solver_ = SAT_Solver(2*(adf.statements.size()+1));
			valid_interpretation(adf, solver_);
			cf_interpretation(adf, solver_);
			bipolar_interpretation(adf, solver_);
			while (true) {
				larger_interpretation(adf, model, solver_);
				bool sat = solver_.solve();
				if (!sat) break;
				vector<uint8_t> new_model = extract_interpretation(adf, solver_.assignment);
				SAT_Solver new_solver_ = SAT_Solver(0);
				adm_verification(adf, new_model, new_solver_);
				if (new_solver_.solve()) {
					vector<int> refinement_clause = refine_neq_clause(adf, new_model);
					solver_.add_clause(refinement_clause);
				} else {
					model = new_model;
				}
			}
			interpretations.push_back(model);
			vector<int> refinement_clause = refine_gt_clause(adf, model);
			solver.add_clause(refinement_clause);
		}
	}
	if (interpretations.size() == 0) {
		interpretations.push_back(interpretation);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_adm_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_com_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	com_bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);
	
	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_prf_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	cf_interpretation(adf, solver);
	bipolar_interpretation(adf, solver);
	if (adf.com) {
		com_bipolar_interpretation(adf, solver);
	}
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) {
			break;
		}
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		cf_interpretation(adf, new_solver);
		bipolar_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve();
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	if (interpretations.size() == 0) {
		interpretations.push_back(interpretation);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_adm_k_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation(adf.statements.size(), Interpretation::Undefined);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	k_bipolar_interpretation(adf, interpretation, solver);
	bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_com_k_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);
	interpretations.push_back(interpretation);

	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	k_bipolar_interpretation(adf, interpretation, solver);
	bipolar_interpretation(adf, solver);
	com_bipolar_interpretation(adf, solver);
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) break;
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_neq_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	return interpretations;
}

vector<vector<uint8_t>> enumerate_prf_k_bipolar(const ADF & adf) {
	vector<vector<uint8_t>> interpretations;
	vector<uint8_t> interpretation = Exists::compute_grd(adf);
	
	SAT_Solver solver = SAT_Solver(2*(adf.statements.size()+1));
	valid_interpretation(adf, solver);
	k_bipolar_interpretation(adf, interpretation, solver);
	bipolar_interpretation(adf, solver);
	if (adf.com) {
		com_bipolar_interpretation(adf, solver);
	}
	larger_interpretation(adf, interpretation, solver);

	while (true) {
		bool sat = solver.solve();
		if (!sat) {
			break;
		}
		vector<uint8_t> model = extract_interpretation(adf, solver.assignment);
		SAT_Solver new_solver = SAT_Solver(2*(adf.statements.size()+1));
		valid_interpretation(adf, new_solver);
		k_bipolar_interpretation(adf, model, new_solver);
		bipolar_interpretation(adf, new_solver);
		while (true) {
			larger_interpretation(adf, model, new_solver);
			bool sat = new_solver.solve();
			if (!sat) break;
			model = extract_interpretation(adf, new_solver.assignment);
		}
		interpretations.push_back(model);
		vector<int> refinement_clause = refine_gt_clause(adf, model);
		solver.add_clause(refinement_clause);
	}
	if (interpretations.size() == 0) {
		interpretations.push_back(interpretation);
	}
	return interpretations;
}

}