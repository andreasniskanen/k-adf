SRCDIR    = src
BUILDDIR  = build
TARGET    = k++adf

SRCEXT    = cpp
ALLSRCS   = $(wildcard $(SRCDIR)/*.$(SRCEXT))
SATSRCS   = $(wildcard $(SRCDIR)/*Solver.$(SRCEXT))
SOURCES   = $(filter-out $(SATSRCS),  $(ALLSRCS))
OBJECTS   = $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

CXX       = g++
CFLAGS    = -Wall -Wno-parentheses -Wno-sign-compare -Wno-literal-suffix -std=c++11
COPTIMIZE = -O3 -flto
LFLAGS    = -Wall
IFLAGS    = -I include

CFLAGS   += $(COPTIMIZE)
CFLAGS   += -D __STDC_LIMIT_MACROS -D __STDC_FORMAT_MACROS

SAT_SOLVER = minisat
MINISAT    = lib/minisat

CFLAGS  += -D SAT_MINISAT
IFLAGS  += -I $(MINISAT)
LFLAGS  += -lz
OBJECTS += $(BUILDDIR)/MiniSATSolver.o $(BUILDDIR)/Solver.oc
SAT_DIR  = $(MINISAT)

$(TARGET): $(OBJECTS)
	@echo "Linking..."
	@echo "$(CXX) $^ -o $(TARGET) $(LFLAGS)"; $(CXX) $^ -o $(TARGET) $(LFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "Compiling..."
	@mkdir -p $(BUILDDIR)
	@echo "$(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<"; $(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<

$(BUILDDIR)/%.oc: $(SAT_DIR)/core/%.cc
	@echo "Compiling core..."
	@echo "$(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<"; $(CXX) $(CFLAGS) $(IFLAGS) -c -o $@ $<

clean:
	@echo "Cleaning..."
	@echo "rm -r $(BUILDDIR) $(TARGET)"; rm -r $(BUILDDIR) $(TARGET)