/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ENCODINGS_H
#define ENCODINGS_H

#if defined(SAT_MINISAT)
#include "MiniSATSolver.h"
typedef MiniSATSolver SAT_Solver;
#else
#error "No SAT solver defined"
#endif

#include "ADF.h"

void two_valued_model(const ADF & adf, SAT_Solver & solver, bool equivalence = true);
void valid_interpretation(const ADF & adf, SAT_Solver & solver);
void larger_interpretation(const ADF & adf, const std::vector<uint8_t> & interpretation, SAT_Solver & solver);
void cf_interpretation(const ADF & adf, SAT_Solver & solver);
void adm_verification(const ADF & adf, const std::vector<uint8_t> & interpretation, SAT_Solver & solver);
void bipolar_interpretation(const ADF & adf, SAT_Solver & solver);
void com_bipolar_interpretation(const ADF & adf, SAT_Solver & solver);
void k_bipolar_interpretation(const ADF & adf, const std::vector<uint8_t> & interpretation, SAT_Solver & solver);

#endif