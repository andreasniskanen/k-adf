/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef FORMULA_H
#define FORMULA_H

#include "Node.h"

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <string>

class Formula {

public:
    node * root;
    std::vector<int> vars;
    std::unordered_set<std::string> vars_set;
    std::unordered_map<std::string,int> var_to_idx;
    std::unordered_map<int,std::string> idx_to_var;
    std::unordered_map<std::string,int> global_var_to_idx;
    int count;
    int global_count;
    int true_var;
    int false_var;    
    
    Formula();
	
    void destroy_formula();
    void destroy_tree(node * leaf);
	
    void parse(std::string input);
    node * parse_node(std::string & input);
	
    std::vector<std::vector<int>> to_cnf(bool global);
    std::vector<std::vector<int>> node_to_cnf(node * node, bool global);

};

#endif
