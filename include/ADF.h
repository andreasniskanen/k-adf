/*!
 * Copyright (c) <2018-2021> <Andreas Niskanen, University of Helsinki>
 * 
 * 
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * 
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * 
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef ADF_H
#define ADF_H

#include "Formula.h"

#include <vector>
#include <map>
#include <unordered_map>
#include <cstdint>

template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std
{
  template<typename S, typename T> struct hash<pair<S, T>>
  {
	inline size_t operator()(const pair<S, T> & v) const
	{
	  size_t seed = 0;
	  ::hash_combine(seed, v.first);
	  ::hash_combine(seed, v.second);
	  return seed;
	}
  };
}

class ADF {
public:

ADF(uint32_t n);
~ADF();

uint32_t n_statements;
uint32_t count;
uint32_t global_count;
bool opt;
bool com;

/*!
 * Statements and acceptance conditions of the ADF instance.
 */
std::vector<uint32_t> statements;
std::vector<std::vector<std::vector<int>>> statement_clauses;

std::vector<Formula> conditions;
std::vector<std::vector<std::vector<int>>> condition_clauses;

std::vector<std::vector<uint32_t>> parents;
std::vector<std::vector<uint32_t>> range;
std::vector<std::vector<uint32_t>> sup_parents;
std::vector<std::vector<uint32_t>> sup_range;
std::vector<std::vector<uint32_t>> att_parents;
std::vector<std::vector<uint32_t>> att_range;
std::vector<std::vector<uint32_t>> redundant_parents;
std::vector<std::vector<uint32_t>> redundant_range;
std::vector<std::vector<uint32_t>> non_bipolar_parents;
std::vector<std::vector<uint32_t>> non_bipolar_range;

std::vector<std::pair<uint32_t,uint32_t>> links;
std::vector<std::pair<uint32_t,uint32_t>> sup_links;
std::vector<std::pair<uint32_t,uint32_t>> att_links;
std::vector<std::pair<uint32_t,uint32_t>> redundant_links;
std::vector<std::pair<uint32_t,uint32_t>> non_bipolar_links;

std::map<std::string,uint32_t> statement_to_idx;
std::map<uint32_t,std::string> idx_to_statement;

int true_var;
int false_var;

std::vector<int> statement_true_var;
std::vector<int> statement_false_var;

std::vector<int> statement_var;

std::unordered_map<std::pair<uint32_t,uint32_t>,int> link_var;
std::unordered_map<int,std::pair<uint32_t,uint32_t>> link_var_to_pair;

std::unordered_map<std::pair<uint32_t,uint32_t>,int> not_false_implies_true_link_var;
std::unordered_map<std::pair<uint32_t,uint32_t>,int> not_true_implies_false_link_var;

void add_statement(std::string statement);
void add_condition(std::string statement, std::string condition);
void initialize_statements();
void initialize_conditions();
void initialize_link_types();

};

#endif
